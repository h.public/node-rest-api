# express-typescript-boilerplate

This is a boilerplate project for creating a TypeScript REST API using Express.js. The project includes:

## Getting Started

### Prerequisites

To use this boilerplate, you'll need to have the following installed on your system:

• Node.js (v14 or higher)
• MongoDB
• Redis

### Installation

Clone the repository:

```
git clone https://gitlab.com/<username>/<project-name>.git
```

Install the dependencies using either NPM or Yarn:

```
# NPM
npm install

# Yarn
yarn
```

### Project Dependencies

#### Dependencies

| Package Name                   | Description                                                          |
| ------------------------------ | -------------------------------------------------------------------- |
| compression                    | Middleware to compress HTTP responses                                |
| express-mongo-sanitize         | Middleware to sanitize MongoDB queries                               |
| ioredis                        | Redis client for Node.js                                             |
| mongoose                       | Object Data Modeling (ODM) library for MongoDB                       |
| pino                           | Fast and low overhead Node.js logger                                 |
| pino-http                      | Pino middleware for logging HTTP requests and responses              |
| pm2                            | Production process manager for Node.js                               |

<details>
  <summary>Dev Dependencies</summary>

| Package Name                      | Description                                                           |
| --------------------------------- | --------------------------------------------------------------------- |
| @rollup/plugin-typescript         | Compile TypeScript in Rollup                                          |
| @types/compression                | TypeScript typings for the compression library                        |
| @types/express                    | TypeScript typings for the Express library                            |
| @types/express-mongo-sanitize     | TypeScript typings for the express-mongo-sanitize library             |
| @types/ioredis                    | TypeScript typings for the ioredis library                            |
| @types/pino                       | TypeScript typings for the pino logger                                |
| rollup-plugin-copy                | Copy files and directories in Rollup                                  |

</details>

## Configuration

Create a .env file in the 'config' folder under root directory of the project and add the following variables:

```
PORT=<port number>
MONGODB_URI=<MongoDB connection string>
JWT_SECRET=<secret key for JWT token>
```
