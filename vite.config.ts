import typescript from '@rollup/plugin-typescript';
import copy from 'rollup-plugin-copy';
import tsconfigPaths from 'vite-tsconfig-paths';
import { defineConfig } from 'vitest/config';

export default defineConfig({
  plugins: [tsconfigPaths()],
  build: {
    target: 'ESNext',
    rollupOptions: {
      input: './src/app.ts',
      plugins: [
        typescript(),
        copy({
          targets: [{ src: 'configs/.env', dest: 'dist/configs/' }],
        }),
      ],
    },
  },
});
