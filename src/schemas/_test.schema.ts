import { OpenAPIRegistry } from '@asteasolutions/zod-to-openapi';
import { HttpStatusCodes } from '@node-common/constants/http-status-codes.js';
import { openApiDocsDir } from '@node-rest/data/restConfig.js';
import { SwaggerHelper } from '@node-rest/helpers/SwaggerHelper.js';
import { zodError } from '@node-rest/schemas/zodError.js';
import z from 'zod';

const apiRegistry = new OpenAPIRegistry();

export const testSchema = z.object({
  test: z.string().trim().min(5),
  // .openapi({
  // 	type: "integer"
  // })
});

apiRegistry.registerPath({
  method: 'post',
  path: '/test',
  summary: 'Testing',
  description: '',
  tags: ['test'],
  request: {
    // params: z.object({}).merge(testSchema),
    body: {
      content: {
        'application/x-www-form-urlencoded': {
          schema: testSchema.merge(zodError),
        },
      },
    },
  },
  responses: {
    ...SwaggerHelper.getResponse(HttpStatusCodes.Success.OK),
    ...SwaggerHelper.getErrorResponseWithExample(HttpStatusCodes.ClientError.BadRequest),
  },
});

SwaggerHelper.addApiPrefixToPaths(apiRegistry, '/api');

SwaggerHelper.writeApiDocumentation(`${openApiDocsDir}/_test.yml`, apiRegistry.definitions);
