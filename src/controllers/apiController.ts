import type { Request, Response } from 'express';
import { z } from 'zod';

const hello = (req: Request, res: Response) => {
  res.send('Hello API World');
};

export const userSchema = z.object({
  v: z.string().trim().min(1),
});

const testGet = (req: Request, res: Response) => {
  res.send('testGet API World');
};

export const APICtrl = {
  hello,
  testGet,
};
