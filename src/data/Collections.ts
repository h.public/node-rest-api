/*
In Mongoose, a collection name is automatically generated based on the model name that is defined using Mongoose's model() method.
By default, Mongoose pluralizes the model name and uses it as the name of the corresponded collection in the database.
*/

export const enum Collections {
  USER = 'users',
}
