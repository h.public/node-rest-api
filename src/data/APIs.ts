const Auth = {
  path: '/auth',
  registerUser: '/auth/user/register',
  userLogin: '/auth/user/login',
  userLogout: '/auth/user/logout',
  forgotPassword: '/auth/user/forgotPassword',
  resetPassword: '/auth/user/resetPassword',
} as const;

const User = {
  path: '/user',
  currentInfo: '/user/currentInfo',
} as const;

export const API = {
  path: '/api',
  Auth,
  User,
} as const;
