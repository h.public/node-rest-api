import { secure } from '@node-common/middleware/secure.js';
import { configApi } from '@node-rest/config.js';
import { errorHandler } from '@node-rest/middleware/error.js';
import {
  pino,
  uncaughtExceptionHandler,
  unhandledRejectionHandler,
} from '@node-rest/middleware/logger.js';
import { swagger } from '@node-rest/middleware/swagger.js';
import compression from 'compression';
import express from 'express';
import mongoose from 'mongoose';
import { routes } from '~/routes/routes.js';

configApi();

const app = express();

app.use(swagger());
app.use(compression());
app.use(secure(app));

process.on('uncaughtException', uncaughtExceptionHandler);
process.on('unhandledRejection', unhandledRejectionHandler);
app.use(pino);

app.use(routes);

// will trigger when next(err)
app.use(errorHandler);

// app.listen(port, () => console.log(`Listening on port ${port}...`))ka

mongoose.set('strictQuery', true);
mongoose
  .connect(process.env.MONGODB_URI as string)
  .then(() => {
    // throw new Error('Intentional crash')

    // User.findOne().then(user => {
    // 	if (!user) {
    // 		const user = new User(
    // 			{
    // 				name: 'admin',
    // 				email: 'admin@shop.com',
    // 				password: '123',
    // 				cart: {
    // 					items: []
    // 				}
    // 			}
    // 		)
    // 		console.log('no user', user)
    // 		user.save()
    // 	}
    // })

    const port = process.env.PORT;
    app.listen(port, () => console.log(`Listening on port ${port}...`));
  })
  .catch();

// @ts-ignore
if (import.meta.hot) {
  // @ts-ignore
  import.meta.hot.on('vite:beforeFullReload', () => {
    mongoose.disconnect();
  });
}
