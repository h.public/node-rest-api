import { HttpStatusCodes } from '@node-common/constants/http-status-codes.js';
import { runError } from '@node-rest/middleware/error.js';
import { validateBodySchema } from '@node-rest/middleware/validateSchema.js';
import express from 'express';
import mongoSanitize from 'express-mongo-sanitize';
import { APICtrl } from '~/controllers/apiController.js';
import { testSchema } from '~/schemas/_test.schema.js';
import authRoutes from '~/services/auth/authRoutes.js';
import userRoutes from '~/services/user/userRoutes.js';

const router = express.Router();

// Sanitize user input to prevent MongoDB operator injection attacks
router.use('/', mongoSanitize());

router.get('/', runError(HttpStatusCodes.ClientError.BadRequest));
router.post('/test', validateBodySchema(testSchema), APICtrl.testGet);

router.use(authRoutes);
router.use('/user', userRoutes);

const apiRoutes = router;
export default apiRoutes;
