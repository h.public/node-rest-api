import { noQuery } from '@node-rest/middleware/noQuery.js';
import type { Request, Response } from 'express';
import express from 'express';

const router = express.Router();

router.get('/', noQuery(), (req: Request, res: Response) => {
  // req.log.info({ message: 'Hello, world!', count: 42 })
  res.send('Hello  World');
});

const indexRoutes = router;
export default indexRoutes;
