import { publicDir } from '@node-common/config/path.js';
import { header } from '@node-rest/middleware/header.js';
import express from 'express';
import { API } from '~/data/APIs.js';
import apiRoutes from './api.js';
import indexRoutes from './index.js';

const router = express.Router();

router.use(express.json()); // application/json
router.use(express.urlencoded({ extended: true }));
router.use(express.static(publicDir));

// router.use(noQuery())
router.use(header);
router.use('/', indexRoutes);
router.use(API.path, apiRoutes);

export const routes = router;
