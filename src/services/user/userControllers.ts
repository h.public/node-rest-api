import AuthSecurityHelper from '@node-rest/helpers/AuthSecurityHelper.js';
import type { Request, Response } from 'express';
import { User } from './userModel.js';

const getUser = (req: Request, res: Response) => {
  res.send('getUser');
};

const addUser = async (req: Request) => {
  const user = new User(req.body);
  const hashPass = await AuthSecurityHelper.hashPassword(user.password);
  user.password = hashPass;
  return await user.save();
};

export const UserApi = {
  getUser,
  addUser,
};
