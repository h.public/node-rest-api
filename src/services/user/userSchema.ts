import { OpenAPIRegistry } from '@asteasolutions/zod-to-openapi';
import { HttpStatusCodes } from '@node-common/constants/http-status-codes.js';
import { openApiDocsDir } from '@node-rest/data/restConfig.js';
import { SwaggerHelper } from '@node-rest/helpers/SwaggerHelper.js';
import { zodError } from '@node-rest/schemas/zodError.js';
import { z } from 'zod';

export const userSchema = z.object({
  name: z.string().trim().min(5).max(50),
});
// .merge(emailSchema)
// .merge(passwordSchema)

type userSchema = z.infer<typeof userSchema>;

const apiRegistry = new OpenAPIRegistry();

apiRegistry.registerPath({
  method: 'get',
  path: '/users',
  // summary: '',
  // description: '',
  tags: ['user'],
  request: {
    params: userSchema.merge(zodError),
  },
  responses: {
    ...SwaggerHelper.getResponse(HttpStatusCodes.Success.OK),
    ...SwaggerHelper.getResponse(HttpStatusCodes.ClientError.BadRequest),
  },
});

SwaggerHelper.writeApiDocumentation(`${openApiDocsDir}/user.yml`, apiRegistry.definitions);
