import { HttpStatusCodes } from '@node-common/constants/http-status-codes.js';
import { runError } from '@node-rest/middleware/error.js';
import { validateBodySchema } from '@node-rest/middleware/validateSchema.js';
import express from 'express';
import { UserApi } from './userControllers.js';
import { userSchema } from './userSchema.js';

const router = express.Router();

router.post('/add', validateBodySchema(userSchema), UserApi.addUser);
router.use('/', runError(HttpStatusCodes.ClientError.BadRequest));

const userRoutes = router;
export default userRoutes;
