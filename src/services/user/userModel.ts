import JWTHelper from '@node-rest/helpers/JWTHelper.js';
import mongoose from 'mongoose';
import { configJWT } from '~/configs/jwt.js';
import { Collections } from '~/data/Collections.js';

const userSchema = new mongoose.Schema({
  name: String,
  email: String,
  password: String,
});

const schemaMethods = (userSchema.methods = {
  generateAuthToken: function () {
    return JWTHelper.generateAuthToken(
      { id: this.id.toString() },
      { expiresIn: configJWT.tokenExpirationTime },
    );
  },
});

export const User = mongoose.model<UserDocument>(Collections.USER, userSchema);

type UserProperties = {
  name: string;
  email: string;
  password: string;
};

type UserDocument = typeof schemaMethods & UserProperties;
