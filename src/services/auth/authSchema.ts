import { OpenAPIRegistry } from '@asteasolutions/zod-to-openapi';
import { HttpStatusCodes } from '@node-common/constants/http-status-codes.js';
import { ResponseMessage } from '@node-rest/constants/ResponseMessage.js';
import { openApiDocsDir } from '@node-rest/data/restConfig.js';
import { SwaggerHelper } from '@node-rest/helpers/SwaggerHelper.js';
import { emailSchema } from '@node-rest/schemas/email.schema.js';
import { passwordSchema } from '@node-rest/schemas/password.schema.js';
import { zodError } from '@node-rest/schemas/zodError.js';
import z from 'zod';
import { API } from '~/data/APIs.js';

export const authSchema = {
  registerUser: z.object({ email: emailSchema, password: passwordSchema }),
  login: z.object({ email: emailSchema, password: passwordSchema }),
  forgotPassword: passwordSchema,
  resetPassword: z.object({
    oldPassword: passwordSchema,
    newPassword: passwordSchema,
  }),
};

const apiRegistry = new OpenAPIRegistry();
export const bearerAuth = apiRegistry.registerComponent(
  'securitySchemes',
  'bearerAuth',
  {
    type: 'http',
    scheme: 'bearer',
    bearerFormat: 'JWT',
  },
);

apiRegistry.registerPath({
  method: 'post',
  path: API.Auth.registerUser,
  summary: 'Allows users to register for a new account on the platform',
  description: '',
  tags: ['auth'],
  request: {
    body: {
      content: {
        'application/x-www-form-urlencoded': {
          schema: authSchema.registerUser.merge(zodError),
        },
      },
    },
  },
  responses: {
    ...SwaggerHelper.getResponse(HttpStatusCodes.Success.Accepted),
    ...SwaggerHelper.getResponse(HttpStatusCodes.ClientError.BadRequest),
    ...SwaggerHelper.getResponse(HttpStatusCodes.ClientError.Conflict, {
      description: 'Email has already been taken.',
    }),
  },
});

apiRegistry.registerPath({
  method: 'post',
  path: API.Auth.userLogin,
  summary: 'Allows users to login on the platform',
  description: '',
  tags: ['auth'],
  request: {
    body: {
      content: {
        'application/x-www-form-urlencoded': {
          schema: authSchema.login.merge(zodError),
        },
      },
    },
  },
  responses: {
    ...SwaggerHelper.getResponse(HttpStatusCodes.Success.Accepted),
    ...SwaggerHelper.getResponse(HttpStatusCodes.ClientError.BadRequest),
    ...SwaggerHelper.getResponse(HttpStatusCodes.ClientError.Unauthorized, {
      description: 'Invalid email or password',
    }),
  },
});

apiRegistry.registerPath({
  method: 'post',
  path: API.Auth.userLogout,
  summary: 'Allows users to logout from the platform',
  description: '',
  security: [{ bearerAuth: [] }],
  tags: ['auth'],
  // request: {
  //   body: {
  //     content: {
  //       'application/x-www-form-urlencoded': {
  //         schema: authSchema.logout.merge(zodError),
  //       },
  //     },
  //   },
  // },
  responses: {
    ...SwaggerHelper.getResponse(HttpStatusCodes.Success.Accepted),
    ...SwaggerHelper.getResponse(HttpStatusCodes.ClientError.BadRequest),
    ...SwaggerHelper.getAuthenticationFailedResponse(),
  },
});

apiRegistry.registerPath({
  method: 'patch',
  path: API.Auth.resetPassword,
  summary: 'Allows users to reset their password.',
  description: '',
  tags: ['auth'],
  security: [{ bearerAuth: [] }],
  request: {
    body: {
      content: {
        'application/x-www-form-urlencoded': {
          schema: authSchema.resetPassword.merge(zodError),
        },
      },
    },
  },
  responses: {
    ...SwaggerHelper.getResponse(HttpStatusCodes.Success.Accepted),
    ...SwaggerHelper.getResponse(HttpStatusCodes.ClientError.BadRequest, {
      description:
        'Bad request' + ' or ' + ResponseMessage.Auth.PASSWORD_INCORRECT,
    }),
    ...SwaggerHelper.getResponse(HttpStatusCodes.ClientError.Unauthorized, {
      description:
        ResponseMessage.Auth.TOKEN_MISSING +
        ' or ' +
        ResponseMessage.Auth.USER_MISSING,
    }),
    ...SwaggerHelper.getResponse(HttpStatusCodes.ClientError.Forbidden, {
      description: ResponseMessage.Auth.INVALID_TOKEN,
    }),
  },
});

SwaggerHelper.addApiPrefixToPaths(apiRegistry, API.path);

SwaggerHelper.writeApiDocumentation(
  `${openApiDocsDir}/auth.yml`,
  apiRegistry.definitions,
);
