import { HttpStatusCodes } from '@node-common/constants/http-status-codes.js';
import RedisHelper from '@node-redis/helpers/RedisHelper.js';
import JWTHelper from '@node-rest/helpers/JWTHelper.js';
import { responseError } from '@node-rest/helpers/ResponseHelper.js';
import { authenticateToken } from '@node-rest/middleware/authentication.js';
import { validateBodySchema } from '@node-rest/middleware/validateSchema.js';
import { Route } from '@node-rest/types/express.js';
import { API } from '~/data/APIs.js';
import { UserApi } from '../user/userControllers.js';
import { User } from '../user/userModel.js';
import { authSchema } from './authSchema.js';
import { forgetPasswordController } from './controllers/forgetPassword.controller.js';
import { loginController } from './controllers/login.controller.js';
import { resetPasswordController } from './controllers/resetPassword.controller.js';

const register: Route = {
  method: 'post',
  path: API.Auth.registerUser,
  middleware: [validateBodySchema(authSchema.registerUser)],
  handler: async (req, res) => {
    let user = await User.findOne({ email: req.body.email });
    if (user)
      return responseError(
        res,
        HttpStatusCodes.ClientError.Conflict,
        'This email has already been taken',
      );

    user = await UserApi.addUser(req);
    const token = user.generateAuthToken();

    await RedisHelper.set(`user:${user.id}:token`, token);

    return res
      .header('Authorization', 'Bearer ' + token)
      .status(HttpStatusCodes.Success.Created)
      .send();
  },
};

const logout: Route = {
  method: 'post',
  path: API.Auth.userLogout,
  middleware: [authenticateToken()],
  handler: async (req, res) => {
    const token = req.headers?.authorization?.split(' ')[1] || '';
    const authData = JWTHelper.decodeAuthData(token);
    await RedisHelper.remove(`user:${authData.id}:token`);
    // console.log(`User with token ${token} logged out`)
    return res.status(HttpStatusCodes.Success.Accepted).send();
  },
};

export const AuthRoutes = {
  register, // Allows users to create a new account by providing their email and password.
  login: loginController, // Allows users to login to their account by providing their email and password. If the credentials are valid, a JWT token is generated and returned to the user.
  logout, // Allows users to logout of their account by removing the JWT token from their client-side storage.
  forgotPassWord: forgetPasswordController, //  Generates a unique password reset token and sends an email to the user with instructions on how to reset their password.
  resetPassword: resetPasswordController, // Validates the password reset token and allows the user to update their password.
  // getMe, // Retrieves information about the currently authenticated user, such as their username, email, and other profile information.
  // putMe, // Updates information about the currently authenticated user, such as their email address or password.
  // deleteMe, // Deletes the account of the currently authenticated user. It invalidates the JWT token and deletes the user's data from the server.
} as const;
