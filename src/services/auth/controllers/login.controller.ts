import { HttpStatusCodes } from '@node-common/constants/http-status-codes.js';
import RedisHelper from '@node-redis/helpers/RedisHelper.js';
import AuthSecurityHelper from '@node-rest/helpers/AuthSecurityHelper.js';
import { responseError } from '@node-rest/helpers/ResponseHelper.js';
import { validateBodySchema } from '@node-rest/middleware/validateSchema.js';
import type { Route } from '@node-rest/types/express.js';
import { API } from '~/data/APIs.js';
import { User } from '~/services/user/userModel.js';
import { authSchema } from '../authSchema.js';

const login: Route = {
  method: 'post',
  path: API.Auth.userLogin,
  middleware: [validateBodySchema(authSchema.login)],
  handler: async (req, res) => {
    const error = () =>
      responseError(res, HttpStatusCodes.ClientError.Unauthorized, 'Invalid email or password');

    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) return error();

    // * this actually should never happened
    // * if it did happens, returning error will induce user to reset password
    if (!user.password) return error();

    const validPassword = await AuthSecurityHelper.comparePassword(user.password, password);
    if (!validPassword) return error();

    await RedisHelper.remove(`user:${user.id}:token`);

    const token = user.generateAuthToken();
    await RedisHelper.set(`user:${user.id}:token`, token);

    return res
      .header('Authorization', 'Bearer ' + token)
      .status(HttpStatusCodes.Success.Accepted)
      .send();
  },
};

export const loginController = login;
