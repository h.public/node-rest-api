import { HttpStatusCodes } from '@node-common/constants/http-status-codes.js';
import { ResponseMessage } from '@node-rest/constants/ResponseMessage.js';
import AuthSecurityHelper from '@node-rest/helpers/AuthSecurityHelper.js';
import JWTHelper from '@node-rest/helpers/JWTHelper.js';
import { responseError, showErrorDetails } from '@node-rest/helpers/ResponseHelper.js';
import { authenticateToken } from '@node-rest/middleware/authentication.js';
import { validateBodySchema } from '@node-rest/middleware/validateSchema.js';
import type { Route } from '@node-rest/types/express.js';
import { API } from '~/data/APIs.js';
import { User } from '~/services/user/userModel.js';
import { authSchema } from '../authSchema.js';

const resetPassword: Route = {
  method: 'patch',
  path: API.Auth.resetPassword,
  middleware: [authenticateToken(), validateBodySchema(authSchema.resetPassword)],
  handler: async (req, res) => {
    const showDetails = showErrorDetails(req.body.zodError);
    const token = req.headers?.authorization?.split(' ')[1] || '';
    const authData = JWTHelper.decodeAuthData(token);
    const user = await User.findById(authData.id);

    if (!user)
      return responseError(
        res,
        HttpStatusCodes.ClientError.Unauthorized,
        showDetails ? ResponseMessage.Auth.USER_MISSING : undefined,
      );

    const { oldPassword, newPassword } = req.body;
    const validPassword = await AuthSecurityHelper.comparePassword(user.password, oldPassword);

    if (!validPassword)
      return responseError(
        res,
        HttpStatusCodes.ClientError.BadRequest,
        ResponseMessage.Auth.PASSWORD_INCORRECT,
      );

    const hashPass = await AuthSecurityHelper.hashPassword(newPassword);
    user.password = hashPass;
    await user.save();

    return res.status(HttpStatusCodes.Success.Accepted).send();
  },
};

export const resetPasswordController = resetPassword;
