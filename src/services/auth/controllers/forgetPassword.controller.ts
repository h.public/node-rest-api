import { HttpStatusCodes } from '@node-common/constants/http-status-codes.js';
import { validateBodySchema } from '@node-rest/middleware/validateSchema.js';
import type { Route } from '@node-rest/types/express.js';
import { API } from '~/data/APIs.js';
import { authSchema } from '../authSchema.js';

const forgetPassword: Route = {
  method: 'post',
  path: API.Auth.forgotPassword,
  middleware: [validateBodySchema(authSchema.resetPassword)],
  handler: async (req, res) => res.status(HttpStatusCodes.Success.Accepted).send(),
};

export const forgetPasswordController = forgetPassword;
