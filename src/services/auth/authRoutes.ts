import { RouteHelper } from '@node-rest/helpers/RouteHelper.js';
import express from 'express';
import { AuthRoutes } from './authControllers.js';

const router = express.Router();
// router.use(
// 	rateLimiter({
// 		max: 3,
// 	}),
// )

// router.post('/user/register', validateBodySchema(authSchema.registerUser), AuthApi.register)
// router.post('/user/login', AuthApi.login)
// router.post('/user/logout', AuthRoutes.logout.handler)
// router.post('/password/reset', AuthApi.resetPassword)

const authRoutes = RouteHelper.parseRoutes(AuthRoutes, router);
export default authRoutes;
